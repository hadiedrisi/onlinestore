﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStoreServices.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int price { get; set; }
        public string Status { get; set; }
        public int Quantity { get; set; }
        public string Producer { get; set; }
        public int FactroNo { get; set; }
        public  DateTime Date { get; set; }
        public Category MyProperty { get; set; }
    }

}
