﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineStoreServices.Model
{
    public class User
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string Lastname { get; set; }
        public DateTime Date { get; set; }
        public string ACL { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
