﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
namespace OnlineStoreServices.Model
{
    public class StoreContext:DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options)
            :base(options)
        {
            
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
